import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { reducer as formReducer } from 'redux-form';
import pamm from './pamm/reducer';

export default combineReducers({
  routing: routerReducer,
  pamm,
  form: formReducer,
});
