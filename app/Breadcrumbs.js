import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { Link, withRouter } from 'react-router-dom';
import Route from 'route-parser';

const isFunction = value => typeof value === 'function';

const getPathTokens = (pathname) => {
  const paths = ['/'];

  if (pathname === '/') return paths;

  pathname.split('/').reduce((prev, curr) => {
    const currPath = `${prev}/${curr}`;
    paths.push(currPath);
    return currPath;
  });

  return paths;
};

function getRouteMatch(routes, path) {
  return Object.keys(routes)
    .map((key) => {
      const params = new Route(key).match(path);

      return { didMatch: params !== false, params, key };
    })
    .filter(item => item.didMatch)[0];
}

function getBreadcrumbs({ routes, location }) {
  const pathTokens = getPathTokens(location.pathname);
  return pathTokens.map((path) => {
    const routeMatch = getRouteMatch(routes, path);

    if (!routeMatch) {
      return false;
    }

    const routeValue = routes[routeMatch.key];
    const name = isFunction(routeValue)
      ? routeValue(routeMatch.params)
      : routeValue;
    return { name, path };
  });
}

const Breadcrumbs = ({ routes, location }) => {
  const breadcrumbs = getBreadcrumbs({ routes, location }).filter(breadcrumb => breadcrumb);

  return (
    <div>
      <ol className="breadcrumb">
        {breadcrumbs.map((breadcrumb, i) => (
          <li key={i} className={cx('breadcrumb-item', { active: i === breadcrumbs.length - 1 })}>
            {i < breadcrumbs.length - 1
              ? (<Link to={breadcrumb.path}>{breadcrumb.name}</Link>)
              : breadcrumb.name
            }
          </li>
        ))}
      </ol>
    </div>
  );
};

Breadcrumbs.propTypes = {
  routes: PropTypes.object, // eslint-disable-line
  location: PropTypes.object, // eslint-disable-line
};


export default withRouter(Breadcrumbs);
