import React from 'react';
import PropTypes from 'prop-types';
import { Link, Redirect, Route, Switch, withRouter } from 'react-router-dom';
import cx from 'classnames';

import Home from '../home/Home';
import Pamm from '../pamm/Pamm';
import Breadcrumbs from './Breadcrumbs';

const routes = {
  '/': 'Dashboard',
  '/pamm': 'PAMM list',
  '/pamm/:pammId/edit': params => `Edit pamm ${params.pammId}`,
};

const App = ({ location: { pathname } }) => (
  <div>
    <div className="sidebar">
      <nav className="sidebar-nav">
        <ul className="nav">
          <li className="nav-item">
            <Link className={cx('nav-link', { active: pathname === '/' })} to="/">
              <i className="icon-speedometer" /> Dashboard
            </Link>
          </li>

          <li className="nav-item">
            <Link className={cx('nav-link', { active: pathname === '/pamm' })} to="/pamm">
              <i className="icon-speedometer" /> Pamm
            </Link>
          </li>
        </ul>
      </nav>
    </div>

    <main className="main">
      <Breadcrumbs routes={routes} />

      <div className="container-fluid">
        <div className="animated fadeIn">
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/pamm" component={Pamm} />
            <Redirect from="*" exact to="/" />
          </Switch>
        </div>
      </div>
    </main>
  </div>
);

App.propTypes = {
  location: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
};

export default withRouter(App);
