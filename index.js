import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';
import store, { history } from './store';
import App from './app/App';

const rootEl = document.querySelector('#root');

render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <App />
    </ConnectedRouter>
  </Provider>,
  rootEl,
);

if (module.hot) {
  module.hot.accept('./app/App', () => {
    // noinspection JSUnresolvedVariable
    const NextApp = require('./app/App').default; // eslint-disable-line
    render(
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <NextApp />
        </ConnectedRouter>
      </Provider>,
      rootEl,
    );
  });
}
