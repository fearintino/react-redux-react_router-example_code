import { applyMiddleware, compose, createStore } from 'redux';
import { routerMiddleware } from 'react-router-redux';
import { createLogger } from 'redux-logger'
import thunk from 'redux-thunk';
import createHistory from 'history/createBrowserHistory';
import rootReducer from './rootReducer';

export const history = createHistory();

const initialState = {};
const enhancers = [];
const middleware = [
  thunk,
  routerMiddleware(history),
];

// if (process.env.NODE_ENV !== 'production') {
// noinspection JSUnresolvedVariable
const devToolsExtension = window.devToolsExtension;

if (typeof devToolsExtension === 'function') {
  enhancers.push(devToolsExtension());
}

const logger = createLogger({
  collapsed: () => true,
  timestamp: false,
  duration: true,
});

middleware.push(logger);
// }


const composedEnhancers = compose(
  applyMiddleware(...middleware),
  ...enhancers,
);

const store = createStore(
  rootReducer,
  initialState,
  composedEnhancers,
);

if (module.hot) {
  module.hot.accept('./rootReducer', () => {
    // noinspection JSUnresolvedVariable
    const nextReducer = require('./rootReducer').default; // eslint-disable-line
    store.replaceReducer(nextReducer);
  });
}

export default store;
