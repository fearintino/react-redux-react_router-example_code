import React from 'react';
import style from './style.css';

const Loader = () => (<div className={style.loader} />);

export default Loader;
