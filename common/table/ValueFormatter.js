import React from 'react';
import PropTypes from 'prop-types';

const ValueFormatter = ({ type, value }) => {
  switch (type) {
    case 'bool': {
      const icon = value === '0' ? 'times' : 'check';
      const color = value === '0' ? 'red' : 'green';

      return (
        <i className={`fa fa-${icon}`} aria-hidden="true" style={{ color }} />
      );
    }
    default:
      return (
        <div>{value}</div>
      );
  }
};

ValueFormatter.propTypes = {
  type: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
};

export default ValueFormatter;
