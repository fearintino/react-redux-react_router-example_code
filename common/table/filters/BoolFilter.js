import React from 'react';
import PropTypes from 'prop-types';

const BoolFilter = ({ filter, onChange }) => (
  <select
    onChange={event => onChange(event.target.value)}
    style={{ width: '100%', fontFamily: 'Arial, FontAwesome' }}
    value={filter ? filter.value : 'all'}
  >
    <option value="all">All</option>
    <option value="1" style={{ color: 'green' }}>&#xf00c; &nbsp; True</option>
    <option value="0" style={{ color: 'red' }}>&#xf00d; &nbsp; False</option>
  </select>
);

BoolFilter.propTypes = {
  filter: PropTypes.object.isRequired, // eslint-disable-line
  onChange: PropTypes.func.isRequired,
};

export default BoolFilter;
