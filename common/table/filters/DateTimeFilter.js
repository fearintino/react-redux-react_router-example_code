import React from 'react';
import PropTypes from 'prop-types';

const DateTimeFilter = ({ onChange, inputWidth = '100%' }) => (
  <input
    style={{ width: inputWidth }}
    type="date"
    onChange={event => onChange(event.target.value)}
  />
);

DateTimeFilter.propTypes = {
  onChange: PropTypes.func.isRequired,
  inputWidth: PropTypes.string,
};

DateTimeFilter.defaultProps = {
  inputWidth: '100%',
};

export default DateTimeFilter;
