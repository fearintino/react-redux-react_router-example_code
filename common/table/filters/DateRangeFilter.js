import React from 'react';
import PropTypes from 'prop-types';
import DateFilter from './DateTimeFilter';

export default props => <DateRangeFilter {...props} />;

class DateRangeFilter extends React.Component {

  static propTypes = {
    onChange: PropTypes.func.isRequired,
  }

  state = { min: null, max: null }

  handleChange = (type, val) => {
    this.setState({ [type]: val }, () => {
      this.props.onChange(this.state);
    });
  }

  render() {
    return (
      <div>
        <DateFilter onChange={val => this.handleChange('min', val)} inputWidth="50%" />
        <DateFilter onChange={val => this.handleChange('max', val)} inputWidth="50%" />
      </div>
    );
  }
}
