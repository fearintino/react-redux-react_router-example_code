import React from 'react';
import { mount } from 'enzyme';
import { mountToJson } from 'enzyme-to-json';
import Table from '../Table';

function setup() {
  const props = {
    rows: [{ firstName: 'Test name' }],
    loading: false,
    columns: [{ Header: 'First Name', accessor: 'firstName' }],
    handleSortChange: jest.fn(),
    handleFilterChange: jest.fn(),
  };

  const enzymeWrapper = mount(<Table {...props} />);

  return { props, enzymeWrapper };
}

describe('Table', () => {
  it('should render self and subcomponents', () => {
    const { enzymeWrapper } = setup();

    expect(mountToJson(enzymeWrapper)).toMatchSnapshot();
  });

  it('should render table header', () => {
    const { enzymeWrapper, props } = setup();
    const header = enzymeWrapper.find('.rt-resizable-header-content');

    expect(header.text()).toBe(props.columns[0].Header);
  });

  it('should render table row', () => {
    const { enzymeWrapper, props } = setup();
    const header = enzymeWrapper.find('.rt-td');

    expect(header.text()).toBe(props.rows[0].firstName);
  });

  it('should call handleSortChange', () => {
    const { enzymeWrapper, props } = setup();
    const table = enzymeWrapper.find('Table').first();

    table.props().handleSortChange();
    expect(props.handleSortChange.mock.calls.length).toBe(1);
  });

  it('should call handleFilterChange', () => {
    const { enzymeWrapper, props } = setup();
    const table = enzymeWrapper.find('Table').first();

    table.props().handleFilterChange();
    expect(props.handleFilterChange.mock.calls.length).toBe(1);
  });
});
