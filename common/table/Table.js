import React from 'react';
import PropTypes from 'prop-types';
import ReactTable from 'react-table';
import cx from 'classnames';
import debounce from 'lodash/debounce';
import './react-table.global.css';

class Table extends React.Component {

  handleFilter = debounce(this.props.handleFilterChange, 500);
  handleSort = debounce(this.props.handleSortChange, 500);

  render() {
    const { rows, columns, loading } = this.props;

    return (
      <ReactTable
        data={rows}
        style={{ height: '100%', textAlign: 'center' }}
        manual
        columns={columns}
        minRows={rows.length}
        loading={loading}
        filterable
        className={cx('-striped -highlight')}
        showPagination={false}
        onSortedChange={this.handleSort}
        defaultPageSize={rows.length}
        onFilteredChange={this.handleFilter}
      />
    );
  }
}

Table.propTypes = {
  rows: PropTypes.array.isRequired, // eslint-disable-line
  columns: PropTypes.array.isRequired, // eslint-disable-line
  loading: PropTypes.bool.isRequired,
  handleSortChange: PropTypes.func.isRequired,
  handleFilterChange: PropTypes.func.isRequired,
};

export default Table;
