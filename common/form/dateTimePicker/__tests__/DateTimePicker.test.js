import React from 'react';
import { shallow } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';
import DateTimePicker from '../DateTimePicker';

describe('<DateTimePicker />', () => {
  it('should render with default props', () => {
    const wrapper = shallow(
      <DateTimePicker
        input={{ value: '', onChange: a => a }}
      />,
    );
    expect(shallowToJson(wrapper)).toMatchSnapshot();
  });
});
