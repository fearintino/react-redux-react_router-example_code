import React from 'react';
import Datetime from 'react-datetime'; // @TODO make pull request with resolve https://fb.me/react-dom-factories
import moment from 'moment';
import 'moment/locale/en-ie';

import './react-datetime.global.css';

export const validator = value => moment(value).isValid();

const DateTimePiker = field => (
  <Datetime
    value={moment(field.input.value)}
    onChange={param => field.input.onChange(typeof param === 'string' ? param : param.format())}
    dateFormat="YYYY-MM-DD"
    timeFormat="HH:mm"
  />
);

export default DateTimePiker;
