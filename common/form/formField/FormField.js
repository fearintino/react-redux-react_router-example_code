import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import cx from 'classnames';

const FormField = ({ name, label, component, type, error }) => (
  <div className={cx('form-group', { 'has-error': error })}>
    <label className="col-sm-2 control-label" htmlFor={name}>{label}</label>
    <div className="col-sm-10">
      <Field className="form-control" name={name} component={component} type={type} />
      {error && <span className="help-block">{error}</span>}
    </div>
  </div>
);

FormField.propTypes = {
  type: PropTypes.string,
  name: PropTypes.string.isRequired,
  error: PropTypes.string,
  label: PropTypes.string.isRequired,
  component: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.string,
  ]).isRequired,
};

FormField.defaultProps = {
  type: '',
  error: null,
};

export default FormField;
