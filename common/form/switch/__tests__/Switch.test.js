import React from 'react';
import { mount } from 'enzyme';
import { mountToJson } from 'enzyme-to-json';
import Switch from '../Switch';

function setup() {
  const props = {
    name: 'test-switch',
    input: {
      onChange: jest.fn(),
    },
  };

  const enzymeWrapper = mount(<Switch {...props} />);

  return {
    props,
    enzymeWrapper,
  };
}

describe('Switch', () => {
  it('should render self and subcomponents', () => {
    const { enzymeWrapper } = setup();

    expect(mountToJson(enzymeWrapper)).toMatchSnapshot();

    const switchProps = enzymeWrapper.find('Switch').props();
    expect(switchProps.name).toEqual('test-switch');
    expect(switchProps.onChange).toBeTruthy();
  });

  it('should call onChange', () => {
    const { enzymeWrapper, props } = setup();
    const switchInput = enzymeWrapper.find('Switch');

    switchInput.props().onChange(false);
    expect(props.input.onChange.mock.calls.length).toBe(1);
  });
});
