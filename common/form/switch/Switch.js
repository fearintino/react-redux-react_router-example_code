import React from 'react';
import PropTypes from 'prop-types';
import Switch from 'rc-switch';

import './switch.global.css';

const SwitchField = ({ name, input }) => (
  <Switch
    name={name}
    checked={input.value && input.value !== '0'}
    onChange={param => input.onChange(param)}
  />
);

SwitchField.propTypes = {
  name: PropTypes.string.isRequired,
  input: PropTypes.object.isRequired, // eslint-disable-line
};

export default SwitchField;
