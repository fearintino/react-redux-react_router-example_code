import axios from 'axios';
import { constants } from './reducer';

export const loadPammList = (filter = [], sort = []) => (dispatch) => {
  dispatch({ type: constants.PAMM_LIST_LOAD_STARTED });

  return axios.get(window.Routing.generate('pamm_list'), { params: { filter, sort } })
    .then(({ data }) => dispatch({ type: constants.PAMM_LIST_LOADED, data }))
    .catch(error => dispatch({ type: constants.PAMM_LIST_LOAD_FAILED, error }));
};

export const loadPamm = pammId => (dispatch) => {
  dispatch({ type: constants.PAMM_FETCH_STARTED });

  return axios.get(window.Routing.generate('pamm_get', { pammId }))
    .then(({ data: pamm }) => dispatch({ type: constants.PAMM_FETCHED, data: pamm }));
};
