import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Route } from 'react-router-dom';

import Edit from './edit/Edit';
import List from './list/List';
import { loadPammList } from './actions';

const Pamm = ({ isFetching, index, loadPammList: loadList }) => (
  <div className="row">
    <div className="col-lg-12">
      <Route path="/pamm/:pammId/edit" component={Edit} />

      <Route
        exact
        path="/pamm"
        render={() =>
          <List index={index} isFetching={isFetching} loadPammList={loadList} />
        }
      />
    </div>
  </div>
);

Pamm.propTypes = {
  index: PropTypes.array.isRequired, // eslint-disable-line
  isFetching: PropTypes.bool.isRequired,
  loadPammList: PropTypes.func.isRequired,
};

export default connect(
  ({ pamm: { isFetching, index } }) => ({ isFetching, index }),
  { loadPammList },
)(Pamm);
