export const constants = {
  PAMM_LIST_LOADED: 'pamm/PAMM_LIST_LOADED',
  PAMM_LIST_LOAD_STARTED: 'pamm/PAMM_LIST_LOAD_STARTED',
  PAMM_LIST_LOAD_FAILED: 'pamm/PAMM_LIST_LOAD_FAILED',
  PAMM_FETCH_STARTED: 'pamm/PAMM_FETCH_STARTED',
  PAMM_FETCHED: 'pamm/PAMM_FETCHED',
};

const initialState = {
  index: [],
  isFetching: false,
  isInitialized: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case constants.PAMM_FETCH_STARTED:
    case constants.PAMM_LIST_LOAD_STARTED:
      return {
        ...state,
        isFetching: true,
      };

    case constants.PAMM_LIST_LOADED:
      return {
        ...state,
        index: action.data,
        isFetching: false,
        isInitialized: true,
      };

    case constants.PAMM_FETCHED:
      return {
        ...state,
        index: state.index.concat([action.data]),
        isFetching: false,
      };

    default:
      return state;
  }
};
