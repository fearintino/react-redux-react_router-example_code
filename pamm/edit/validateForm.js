import { validator as dateTimeValidator } from '../../common/form/dateTimePicker/DateTimePicker';

export default (values) => {
  const errors = {};

  if (!dateTimeValidator(values.dt_creation)) {
    errors.dt_creation = 'Invalid date';
  }

  return errors;
};
