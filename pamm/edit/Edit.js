import React from 'react';
import PropTypes from 'prop-types';
import find from 'lodash/find';
import { connect } from 'react-redux';
import { Form, getFormSyncErrors, reduxForm } from 'redux-form';

import Loader from '../../common/loader/Loader';
import Switch from '../../common/form/switch/Switch';
import FormField from '../../common/form/formField/FormField';
import DateTimePiker from '../../common/form/dateTimePicker/DateTimePicker';
import validate from './validateForm';
import { loadPamm } from '../actions';

class Edit extends React.Component {

  static propTypes = {
    pamm: PropTypes.object, // eslint-disable-line
    formErrors: PropTypes.object, // eslint-disable-line
    pammId: PropTypes.string.isRequired,
    loadPamm: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    reset: PropTypes.func.isRequired,
    valid: PropTypes.bool.isRequired,
    pristine: PropTypes.bool.isRequired,
    submitting: PropTypes.bool.isRequired,
  }

  componentWillMount() {
    const { pamm, pammId } = this.props;

    if (!pamm) {
      this.props.loadPamm(pammId);
    }
  }

  submit = (values) => {
    console.log(values); // eslint-disable-line
  }

  render() {
    const { pamm, handleSubmit, pristine, reset, submitting, formErrors, valid } = this.props;

    return (
      <div>
        {(!pamm && <Loader />) || (
          <div>
            <h2>PAMM {pamm.id} edit</h2>

            <Form className="form-horizontal" onSubmit={handleSubmit(this.submit)}>

              <FormField
                name="dt_creation"
                label="Date creation"
                component={DateTimePiker}
                error={formErrors && formErrors.dt_creation}
              />

              <FormField name="public" label="Is public" component={Switch} />

              <FormField name="login" label="Login" component="input" type="text" />

              <FormField name="description" label="Description" component="textarea" />

              <button type="submit" className="btn btn-default" disabled={pristine || submitting || !valid}>
                Submit
              </button>
              <button type="button" className="btn btn-default" disabled={pristine || submitting} onClick={reset}>
                Undo Changes
              </button>
            </Form>
          </div>
        )}
      </div>
    );
  }
}

export default connect(
  (state, { match: { params: { pammId } } }) => {
    const pamm = find(state.pamm.index, { id: pammId });

    return { pamm, pammId, initialValues: pamm, formErrors: getFormSyncErrors('edit_form')(state) };
  },
  { loadPamm },
)(reduxForm({ form: 'edit_form', validate })(Edit));
