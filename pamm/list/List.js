import React from 'react';
import PropTypes from 'prop-types';
import { Scrollbars } from 'react-custom-scrollbars';

import tableConfig from './tableConfig';
import Table from '../../common/table/Table';

class List extends React.Component {

  static propTypes = {
    index: PropTypes.array.isRequired, // eslint-disable-line
    isFetching: PropTypes.bool.isRequired,
    loadPammList: PropTypes.func.isRequired,
  }

  state = { sort: [], filter: [] }

  componentWillMount() {
    this.props.loadPammList();
  }

  handleFilter = (filter) => {
    this.setState({ filter });
    this.requestData();
  }

  handleSort = (sort) => {
    this.setState({ sort });
    this.requestData();
  }

  requestData = () => {
    const { sort, filter } = this.state;
    this.props.loadPammList(filter, sort);
  }

  render() {
    const { isFetching, index } = this.props;

    return (
      <div className="card">
        <div className="card-header">
          <i className="fa fa-align-justify" /> PAMM list
        </div>
        <div className="card-block">
          <div>
            <Scrollbars style={{ height: 300 }}>
              <Table
                rows={index}
                columns={tableConfig}
                loading={isFetching}
                handleSortChange={this.handleSort}
                handleFilterChange={this.handleFilter}
              />
            </Scrollbars>
          </div>
        </div>
      </div>
    );
  }
}

export default List;
