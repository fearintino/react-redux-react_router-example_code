import BoolFilter from '../../common/table/filters/BoolFilter';
import DateTimeFilter from '../../common/table/filters/DateTimeFilter';
import DateRangeFilter from '../../common/table/filters/DateRangeFilter';
import ValueFormatter from '../../common/table/ValueFormatter';

const column = (title, accessor) => rest => ({ accessor, Header: title, ...rest });

const boolColumn = (title, accessor) => column(title, accessor)({
  type: 'bool',
  Cell: ({ column: { type }, value }) => ValueFormatter({ type, value }),
  Filter: BoolFilter,
});

const dateColumn = (title, accessor) =>
  column(title, accessor)({ minWidth: 160, Filter: DateTimeFilter });

const dateRangeColumn = (title, accessor) =>
  column(title, accessor)({ minWidth: 320, Filter: DateRangeFilter });

export default [
  column('Client Id', 'client_id')(),
  boolColumn('Is public', 'public'),
  boolColumn('Approved docs', 'invest_enable'),
  boolColumn('Is test', 'test'),
  boolColumn('Is beneficiary', 'benefit'),
  boolColumn('Is disableCalc', 'calc_disabled'),
  boolColumn('Show trading statements', 'statement_show'),
  boolColumn('Adjustment pending enabled', 'adjustment_pending_enabled'),
  boolColumn('Adjustment opens enabled', 'adjustment_opened_enabled'),
  dateRangeColumn('Date creation', 'dt_creation'),
  dateRangeColumn('Date activation', 'dt_activation'),
  dateColumn('Old date publication', 'dt_set_name'),
  dateColumn('Date liquidation', 'dt_liquidation'),
  dateColumn('Declaration min date starting', 'declaration_date_limit'),
  dateColumn('Date audit', 'audit_external'),
];
