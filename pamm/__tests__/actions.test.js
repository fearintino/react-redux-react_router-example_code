import moxios from 'moxios';
import thunk from 'redux-thunk';
import configureMockStore from 'redux-mock-store';

import * as actions from '../actions';
import { constants } from '../reducer';

const middleware = [thunk];
const mockStore = configureMockStore(middleware);

const filterTestFn = jest.fn();

describe('async actions', () => {
  beforeEach(() => {
    moxios.install();
  });

  afterEach(() => {
    moxios.uninstall();
  });

  it('loadPammList action', () => {
    moxios.requests.mostRecent();

    moxios.wait(() => {
      const request = moxios.requests.mostRecent();
      request.respondWith({
        status: 200,
        response: { pamm: ['some pamm'] },
      });
    });

    filterTestFn.mockReturnValueOnce('https://test.com/api/pamms');

    global.Routing = { generate: filterTestFn };

    const expectedActions = [
      { type: constants.PAMM_LIST_LOAD_STARTED },
      { type: constants.PAMM_LIST_LOADED, data: { pamm: ['some pamm'] } },
    ];
    const store = mockStore({ pamm: [] });

    return store.dispatch(actions.loadPammList()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it('loadPamm action', () => {
    moxios.requests.mostRecent();

    moxios.wait(() => {
      const request = moxios.requests.mostRecent();
      request.respondWith({
        status: 200,
        response: { id: 3 },
      });
    });

    filterTestFn.mockReturnValueOnce('https://test.com/api/pamm/3');
    global.Routing = { generate: filterTestFn };

    const expectedActions = [
      { type: constants.PAMM_FETCH_STARTED },
      { type: constants.PAMM_FETCHED, data: { id: 3 } },
    ];
    const store = mockStore({});

    return store.dispatch(actions.loadPamm(3)).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
