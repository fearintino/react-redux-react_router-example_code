import reducer, { constants } from '../reducer';

describe('pamm reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      index: [],
      isFetching: false,
      isInitialized: false,
    });
  });

  it('should handle PAMM_FETCH_STARTED', () => {
    expect(
      reducer({ isFetching: false }, { type: constants.PAMM_FETCH_STARTED }),
    ).toEqual({ isFetching: true });
  });

  it('should handle PAMM_LIST_LOAD_STARTED', () => {
    expect(
      reducer({ isFetching: false }, { type: constants.PAMM_LIST_LOAD_STARTED }),
    ).toEqual({ isFetching: true });
  });

  it('should handle PAMM_LIST_LOADED', () => {
    expect(
      reducer(
        {
          index: [],
          isFetching: true,
          isInitialized: false,
        },
        { type: constants.PAMM_LIST_LOADED, data: [{ id: 1 }] },
      ),
    ).toEqual({
      index: [{ id: 1 }],
      isFetching: false,
      isInitialized: true,
    });
  });

  it('should handle PAMM_FETCHED', () => {
    expect(
      reducer(
        {
          index: [{ id: 1 }],
          isFetching: true,
        },
        { type: constants.PAMM_FETCHED, data: { id: 2 } },
      ),
    ).toEqual({
      index: [{ id: 1 }, { id: 2 }],
      isFetching: false,
    });
  });
});
